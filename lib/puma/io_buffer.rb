# frozen_string_literal: true

module Puma
  class IOBuffer < Array
    alias reset clear
  end
end
