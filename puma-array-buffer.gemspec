Gem::Specification.new do |spec|
  spec.name          = "puma-array-buffer"
  spec.version       = "0.0.1"
  spec.authors       = ["Robert May"]
  spec.email         = ["rmay@gitlab.com"]

  spec.summary       = %q{Blarp}
  spec.description   = %q{Blorp}
  spec.homepage      = "https://gitlab.com/robotmay_gitlab/puma-array-buffer"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/robotmay_gitlab/puma-array-buffer"
  spec.metadata["changelog_uri"] = "https://gitlab.com/robotmay_gitlab/puma-array-buffer"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
